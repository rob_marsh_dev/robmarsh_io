---
title: "Music"
date: 2018-01-20T16:10:26Z
draft: false
type: "static"
---

<p>For most of my life I have been composing and performing music that spans genres from thrash metal to ambient electronic. More recently this has expanded to incorporate music production when performing music as a solo act. Some of my major influences include Adam Young, Armin Van Buuren and Damon Albarn. Below is a compilation of some of my favourite songs that I have recorded. I hope you enjoy listening to them.</p>

<hr />

<iframe width="100%" height="465" scrolling="no" frameborder="no"
    src="https://w.soundcloud.com/player/?url=https://soundcloud.com/robertmarshmusic/sets/compilation&auto_play=false&buying=false&liking=true&download=true&sharing=true&show_artwork=true&show_comments=true&show_playcount=false&show_user=true&hide_related=false&visual=true&start_track=0&callback=true">
</iframe>
