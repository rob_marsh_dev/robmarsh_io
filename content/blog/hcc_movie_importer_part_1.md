---
title: "HCC: Movie Importer - Part 1"
date: 2017-08-10T17:58:49+01:00
draft: false 

categories: ["Programming", "Elixir"]
comments: false
tags: ["elixir", "hcc", "programming"]
---

This series timelines my development on the Hull Cinema Club web app I am writing. If you haven’t read my introduction to the project, you can [find it here]({{< relref "blog/hull_cinema_club_introduction.md" >}}).

The first step when developing the movie importer was going to be grabbing the cinema listings from an external API. Though the choices were fairly limited I decided to go with [CineList](https://api.cinelist.co.uk/), mainly due to the fact that it allowed me to query listings days in advance. In order to implement the functionality I would require an API wrapper and a method of parsing the response into a format Elixir would like. For this I decided to use [Tesla](https://github.com/teamon/tesla)  because it was well documented and placed on middleware (something I am very used to coming from writing/maintaining .NET APIs). This would likely be slight overkill as the requirements for my wrapper were very simple. There would be no authorisation required and I would only ever be accessing a single endpoint. Despite this I felt it best to utilise a library that gave me room to expand should these requirements ever change. My implementation of the API wrapper can be seen below…

```
defmodule CineList do 
  use Tesla

  plug Tesla.Middleware.BaseUrl, "http://www.cinelist.co.uk/"
  plug Tesla.Middleware.JSON

  def cinema_listings(cinema_id) do
    get("/get/times/cinema/#{cinema_id}")
  end
end
```

This is a near exact copy of the example that can be found in the GitHub documentation and it simply allows me to query the API using a cinema ID that I pass into the method. This is made even more simple as I will start off using only a single ID for our local Vue cinema. 

Listings are returned from the API as a list which gave me a good opportunity to apply some good ol’ recursion. An Elixir list can either be empty or contain a head/list, and by popping the head we are able to perform on it before recursing using the tail. Once the head of the list is empty then that means that list is empty and the result may be returned.  The following is an example of this…

```
def import_listings([]), do: []
def import_listings([head | tail]) do
  {_, movie} = import_movie(head["title"])
  import_listing(movie.id, head["times"])
  import_listings(tail)
end
```

The head of the list contains two properties; a title and an array of times. The are used to populate a movie, and listing respectively before the tail (the rest of the list) is passed into the same method to do it on the next item. It is my understanding that Elixir has built in functions for handling this sort of interaction without having to define two individual functions, however I set myself a target to only implement concepts I understand and this seemed a worthwhile exercise to reiterate some of the fundamentals of the language.

### The database, schemas and migrations

From here the information needed to be stored in a database so that it could later be accessed by the Hull Cinema Club app. [Ecto](https://github.com/elixir-ecto/ecto) is the go-to database wrapper in Elixir as it provides some handy utilities for writing queries, creating schemas and performing migrations. To start off with I would store the movie name and the date/time of the listing in the database. This involved creating a schema and a migration that mapped the same properties and performing a database insert. At this point the basic implementation was done, but I was duplicating the movie names and should have been referring to them via an ID instead which would required me to use some of the cooler attributes to define relationships.

By using both `:belongs_to` and `:has_many` I was able to define a one-to-many relationship between movies and their listings. This is pretty standard database design but it is nice to know that Elixir has decent tools for handling it so that I don’t have to mess about with any external database tools. The inclusion of migrations also ensured that any changes to my database were performed in my code. This is a very developer-centric approach and I appreciated it.

### Scheduled imports

In order to ensure that the listings were up to date it was going to be necessary to run the importer on a regular basis. The amount of work being performed is fairly minimal but cinemas are unlikely to change their listings on a regular basis and so it is acceptable to make the requests once per day. In order to achieve this I would need functionality similar to that of a cron job. Thankfully Erlang (and by extension, Elixir) has a built in module called GenServer that can be utilised to perform tasks at intervals. My implementation of this can be seen below…

```
defmodule MovieImporter.Sync do
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    :timer.send_interval(24 * 60 * 60 * 1000, self(), :work) # Sets listings to be improted every 24 hours
    {:ok, state}
  end

  def handle_info(:work, state) do
    MovieImporter.init(:ok)
    {:noreply, state}
  end
end
```

This function works by periodically sending a message to itself, which in this case is the atom :work. However, in this scenario the contents of the atom is irrelevant, as all I want to happen is to have the message acknowledged and run the function within handle_info. 

The initial implementation made use of a similar Elixir method `Process.send_after/3`, however this would only reschedule the task *after* the work had been completed. This meant that the time that the job was scheduled for would be offset by the time it took to complete it with each import. By not using this method it does make me at risk of overloading, as the system could send the next message before the function of the previous one has finished executing, however this is extremely unlikely as the task is scheduled for every 24 hours and the import typically takes less than 5 seconds.

The only downside to this approach is that the task will always be executed 24 hours after the application is started. In order for me to ensure that it runs at midnight I will have to start it manually at that time or find an application that will handle scheduled deployments for me. This will no doubt be something that I come back to address once my understanding of the language improves.

### Next Steps

The basic functionality for the Movie Importer has been implemented but there are still a number of additions that should be made before moving on. Firstly, the application only imports from a single cinema but it is not uncommon for us to go to another if it has better viewing times. It will be necessary for the system to make repeated requests using all the cinemas in my local area. Further down the line it might be nice to allow the system to be used outside of Hull but this is not a concern for now.

Next up is the way that the system handles failure. It is not uncommon for Vue to put on showings for foreign films and stage shows which may not be documented in the Online Movie Database API. The system does currently check for this but seeing as Hull Cinema Club is going to be reliant on this information, it would be preferable if it notified me of the import status via email. This way I can either update the information mainly or, more likely, ignore it.

Lastly, the project directory is starting to look a little messy as I am unsure of conventions for organising it. Furthermore, most of the code is dumped into one file and it would be nice (for my OCD’s sake) if it were split up into separate modules.

### Follow my work

If you are interested to see how I am getting on then the repository for the Movie Importer [can be found here](https://bitbucket.org/rob_marsh_dev/movie_importer).


