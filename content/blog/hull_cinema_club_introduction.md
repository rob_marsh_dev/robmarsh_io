---
title: "Hull Cinema Club - A Learning Exercise"
date: 2017-08-07T10:54:35Z
draft: false

categories: ["Programming", "Elixir"]
comments: false
tags: ["elixir", "hcc", "programming"]
---

Once a week myself and some friends go to the our local multiplex to see a movie. As with any group of friends we are extraordinarily indecisive and have a hard time communicating which films we want to see. Movies are often decided on at the last minute and dropouts are common. Pretty annoying considering we need an even number (thanks Meerkat Movies). 

If we were going to keep participation levels high, our organisation needed improvement, this is where the Hull Cinema Club concept came from. It would be somewhere we could all vote for which movie to see that week - we could maybe even review them if we were feeling fancy.  The seed was planted, the domain was bought and I was good to go.

Whenever I’m working on a side-project I take it as an opportunity to learn something new and I’ve recently taken an interest in Elixir.  I first encountered it at work and it’s a world away from what I’ve done before; both in terms of syntax and paradigm but the potential performance increase, ease of concurrency and a recent rise in popularity had me intrigued. It also benefits from a well-established framework for developing web applications and APIs called Phoenix. 

This would be what I’d use to build my application.

You know what it’s like; too many things to learn and too little time. Because of this my side projects often fall by the way side so I set myself a number of targets to make sure I keep on track this time.

1. **Not everything I write needs to be perfect and account for every scenario ever.** My instinct when starting a new project is to plan it to death and then plan some more. I can’t see the future and I should try to plan for what I know and nothing else. As the saying goes, “done is better than perfect”.
2. **Take small incremental steps.** Commit little and often whilst trying to set lots of little milestones that can be achieved through no more than a few hours work. This should help keep me focused prevent the next goal seeming out of reach.
3. **Implement only concepts I’ve studied.** I’ve read the beginners handbook on Elixir and understand the core concepts, syntax and data types but that’s about it. Stack Overflow may have the most complete solution, however it fails to be educational if I don’t understand it. This may well mean that I write bad code but so be it. I’ll never start otherwise.


### The First Problem
Initial planning for this project started sometime around the turn of the year, whilst I was at my old job. I started drawing up some mock ups and researched sources for film data as past projects had taught me that there was nooooo way I was going to do it manually. I soon found that there simply wasn’t an easy to access, competent source of cinema listings in the UK. Some had been promised, others were hidden behind a paywall and the rest didn’t have the depth of information I was hoping for. This initially put me right off the idea of making Hull Cinema Club. I was looking for quick wins and because it was going to be time consuming and difficult to get started, I wouldn’t be able to achieve that. It was only recently that I decided to think “less big picture” and split HCC up into two projects in order to solve my issue.

### The Movie Importer

![Browse Image](img/blogs/hcc_01/browse.png)


This basic mockup highlighted what I thought I needed in order to make a web app I could be proud of. It would need the title, a description, a poster and to show listings from all cinemas within a nearby radius. Needless to say, an API with this information didn’t exist but I  found what I needed in two types of API:

1. **Cinema Listings** - There were a couple of APIs that parse the HTML of other websites in order to send back basic data about film times.
2. **Movie databases** - These contained huge amounts of information on movies but no information on whether they were being shown at cinemas or what times they were on.

Both of these datasets contained necessary information and so I made the decision to make a separate application for compiling my information. This would mean that half the project would already be completed before starting on the API and I wouldn’t have to worry about my database design.

The Movie Importer will be a standalone Elixir application that runs nightly and pulls the cinema listings for the next 3-5 days along with any relevant information about the movie that I can grab from other sources. This is by no means a perfect solution and means I will have to write some ugly code for checking for duplicates and matching across APIs using the movies name (yuck) but it ensure I have the quality of data that I need without having to enter it myself. Besides, at least this way the nasty code should be kept out the API!

### Conclusion
Hopefully this has served as an interesting read following my journey to becoming a better developer. There is nothing to say that this project will ever see the light of day but I am hopeful. It could really benefit myself and my friends and it would be really fun to see them using it . Development has already started and I will be writing a follow up very shortly to outline some of the early steps I’ve made.

Alternatively, if you want to see what I’m writing directly have a look at the repo. If you have any advice for a novice Elixir developer then feel free to share as I’m always looking to improve my work.


