---
title: "HCC: Movie Importer - Part 2"
date: 2017-09-18T18:10:16+01:00
draft: false 

categories: ["Programming", "Elixir"]
comments: false
tags: ["elixir", "hcc", "programming"]
---

This series timelines my development on the Hull Cinema Club web app I’m writing. If you haven’t read my introduction to the project, you can [find it here]({{< relref "/blog/hull_cinema_club_introduction.md" >}})

At the time of finishing my previous blog the basic functionality for the movie importer had been implemented (even if some of the code would make you wince). The next step was to add support for multiple cinemas as we will often move around if timings are better. Seeing as the cinemas are fixed and wouldn’t need to be updated I decided to just seed this data. The process for doing this in Elixir involves using the Ecto library to manually insert the entries. There is a short script for this which can be run using the Elixir build tool. There’s no complicated libraries or compiled code so I found it really easy and hassle free.

I needed to iterate over each cinema in order to retrieve their listings, and as a .NET developer I found myself looking for an alternative to ‘foreach’. Stack Overflow informed me that [Enum.each](https://hexdocs.pm/elixir/Enum.html#each/2) would be the simplest way to perform this operation. It takes a enumerable and iterates over it, each time passing the value into a function that is defined as the second argument. The following code pulls every cinema from the database and prints their names…

```
Cinema |>
Repo.all |>
Enum.each(fn (cinema) ->
	IO.inspect cinema.name
end)
```

I would then repeat this process for each of the five following days so that the importer wouldn’t pull today’s listings only. I suspect it is possible that there is a more “functional” way of doing this and that my head is still wired to think in .NET terms but it fit the bill and was tidier than the approaches I had previously been using.

### Email notifications
Early testing of the movie importer had shown that querying the movie details from OMDb by name had not always gone as planned. It was always going to be necessary for me to be notified whenever an import failed and the easiest way to notify myself would be via email. Thankfully, there was a library that would help me achieve just that.

[Bamboo](https://github.com/thoughtbot/bamboo) is a adaptable library for creating and sending emails via a number of different methods including SMTP and external services such as Mailgun and Mandrill. To send an email is as simple as defining it…

```
def missing_movie_details(movie) do
    new_email()
    |> to("rob.dmind@gmail.com")
    |> from("notifications@hullcinemaclub.com")
    |> subject("Missing Movie Information for #{movie.name}")
    |> text_body("Missing movie information for #{movie.name} with id #{movie.id}. Please rectify ASAP.")
  end
```

…and sending it…

`movie |> Email.missing_movie_details |> Mailer.deliver_later`

The process gets a little more complicated with HTML emails, however as the email was only ever going to get sent to me I figured this would be surplus to requirements at this stage.

Another nice feature of Bamboo is how you can define different adapters for your development and production environments. My prod.exs file makes use of the SMTP adapter to send live emails, whereas in my dev.exs I can use a local adapter that stores the email in memory so that my inbox does not get flooded. 

![Email Hell](/img/blogs/hcc_03/email_hell.png)

This is my life now…

Once the project gets to the point where I am sending out notification emails to users it might be nice to separate out the emails into their own separate application. Certainly something to consider down the line.

### API woes

The newly implemented email notifications were triggered whenever no data was found for a particular film. The notifications were flooding in and they did a good job of highlighting just how many movie imports were failing. Furthermore, upon closer inspection of the database it was clear that a number of movie imports were silently failing (eg. they were pulling information it was just for the wrong movie). The vast majority of these failures were caused by modern remakes of old films, such as It or Beauty and the Beast. Without specifying the year that they were released the OMDb API would default to the earliest version of the movie which was rarely going to be correct. Unfortunately, it would also have been a gamble to assume that the movie was released in the current year and occasionally films will get re-releases (this was proven by a recent showing of Gladiator at Cineworld). I knew from the offset that it would be problematic querying using just the movie name but didn’t realise how badly it would affect the imported data. Of the initial 33 movies imported, 6 were incorrect - that’s 15% of my data! 

The inaccuracy of my data led me to look for an alternative. I started by looking at how [Find Any Film](https://www.findanyfilm.com/) populated their data and found that they were calling an API. The information being returned was a lot more detailed than CineList and crucially contained the year that the movie was released. It was then that I made the decision to switch out the API in order to improve the quality of my data.

The results from the Find Any Film API proved to be a bit of a mixed bag. It was correctly identifying modern adaptations of movies but was just plain wrong in some instances. The most obvious example was The Hitman’s Bodyguard that it claimed was released in 2016 despite it having a worldwide release in August 2017. In total the number of incorrect imports had improved and were down from 6 to 4. Though this was not what sold me on the use of this API in place of CineList; by querying OMDb using the year that it was released it would either give me the correct movie or fail - there would be no ambiguity caused by movies with the same name. This meant that if a movie failed, I would know about it and wouldn’t potentially be publishing incorrect information.

The problems that I’ve been having demonstrate the issues with relying on third party data for my application. Unfortunately with the amount of listings being gathered there is no practical way that I can input them manually and so the best I can do is to try and mitigate the damage. In this case that means making wrongly imported data as easy to update as possible. This process will no doubt be improved in the future but at the time of writing I’ve added a function that queries OMDb using the IMDb id of the movie. This means that I just need to specify the id rather than entering in the details by hand. 

### The refactor
With all these recent changes my codebase was starting to look a little messy and could use a tidy up. Whilst searching for tips on refactoring in Elixir I came across [Credo](https://github.com/rrrene/credo), a code analysis tool that recommends best practices and promotes consistent code.

![Credo Image](img/blogs/hcc_03/credo_full.png)

It was able to highlight a number of issues with the code I was writing and was even able to advise on how to fix some of my problems (always nice to have a library do my work for me). Most changes were fairly minor and involved using aliases to exclude module names and slim down my files, however some highlighted areas of code that should be refactored. The most obvious one confirmed my suspicion that I should not be nesting each loops though I have been unable to conjure up a better alternative for the time being. 

I will definitely be making use of Credo on a regular basis to ensure that I’m not making sloppy mistakes.

### Initial deployment
Time to deploy the movie importer onto a live server and see how it fares in the wild. [Edeliver](https://github.com/edeliver/edeliver) allows you to build and deploy Elixir applications whilst providing a useful commands that allow you to remotely stop and start them. In practice I actually found it to be a little tricky to get up and running, partly due to it’s dependency on git. Anything that wasn’t committed to my repository was ignored including secret files that contained credentials for my live database and API keys. There were workarounds for this in the documentation but it seemed strange to omit such a common requirement from the standard build process.

Another annoyance I found was to do with the mix build tool and running my database migrations on a live server. It might have been naivety but I was unable to find a way to run them without changing the development config on my local machine to point to the production database and execute the command locally. This isn’t necessarily a huge problem but it does seem a shame that I can’t perform all the operations I need on the server itself.

All in all I found the deployment process to be a little bit of a faff. It took around 4-5 commits to actually get the application to a point where it would build and start without any issues. That being said, once the initial version was up and running most of the subsequent releases were easy enough.

### What next?
The importer has now been running in a production environment for 3 days and has imported listings daily while sending out emails when it cannot get the movie information. Though the success rate of the movie data imports are lower than I would like, I appreciate that this was an inevitable problem whilst relying so heavily on services provided by other people. 

The first major obstacle for the project has been overcome and I can turn my attention to actually building the Hull Cinema Club web app. Originally the plan was to build an API for HCC and use a frontend Javascript framework for most of the functionality, however I may reconsider this and use Phoenix’s server rendered templates. The less Javascript I have to write the better!

If you would like to have a look at the code behind the movie importer you can find my repository [here](https://bitbucket.org/rob_marsh_dev/movie_importer). Any advice or feedback is very welcome.