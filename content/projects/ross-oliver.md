---
title: "Ross Oliver"
date: 2018-01-22T06:45:16Z
draft: false

summary: "A Shopify e-commerce platform for selling luxury shoes"
image: "ross-oliver.jpg"
carousel_images: ["ross-oliver.jpg"]
colour: "#5f4173"
year: 2017
project_type: "Commercial"
technology: "Shopify"
repo: ""

# Images

---

Ross Oliver was a commercial project undertaken whilst running my own company, Three Point Alpha. We needed to develop an e-commerce store for a luxury footwear retailer and made use of [Shopify](https://www.shopify.co.uk/) in order to achieve it. The project required working closely with an external designer and the client to establish a design and utilising [Shopify’s Liquid templating language](https://github.com/Shopify/liquid)  to create a responsive, high-end store. Ross is putting together another couple of collections together and is hoping to launch his brand this year!