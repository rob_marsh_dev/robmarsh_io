---
title: "Movie Importer"
date: 2018-01-27T09:32:12Z
draft: false

summary: "An importer for cinema listings and movie metadata built in Elixir."
image: ""
carousel_images: ["data.png"]
class: "project-list__movie-importer"
year: 2018
project_type: "Personal"
technology: "Elixir"
repo: "https://bitbucket.org/rob_marsh_dev/hull_cinema_club"

# Images

---

Movie Importer is a personal project I undertook to become more familiar with [Elixir](https://elixir-lang.org/) after being exposed to it at work. The role of the application is to perform a scheduled daily query to third party services in order to gather information on local cinema listings and movie metadata. This will eventually be the foundation for a social web platform for organising cinema trips between friends (something I try and do on a weekly basis). 

The project capitalises on a number of great open source libraries to aid in parsing JSON, sending emails and ensuring that I follow Elixir programming standards. 

This project is viewed as a learning exercise in order to better understand functional programming and, though rough in places, shows a keenness to expand my knowledge and explore new languages and technologies. Whilst developing the application I wrote a series on Medium detailing my experiences learning Elixir, the first of which can be found [here]({{< relref "blog/hull_cinema_club_introduction.md">}}). The source code can also be viewed by clicking the Bitbucket link below.