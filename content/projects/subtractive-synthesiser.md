---
title: "Subtractive Synthesiser"
date: 2018-01-27T09:43:25Z
draft: false

summary: "A synthesiser built using C++."
image: ""
carousel_images: ["ss_logo.png", "ss_application.png"]
class: "project-list__subtractive-synthesiser"
year: 2015
project_type: "Academic"
technology: "C++"
repo: "https://bitbucket.org/rob_marsh_dev/subtractive_synthesiser"

# Images

---

'Subtractive Synthesiser' is my self-proposed dissertation project for the final year of university. As a producer myself, I wanted to create an application that mimicked software that I use to produce my music.

The application is written in C++ for efficiency, whilst implementing a framework called [JUCE](https://juce.com/) for UI components and MIDI interfacing.

Digital signal processing techniques are used for waveform representation, filtering and envelope generation.