---
title: "Retained Availability"
date: 2018-01-27T09:40:12Z
draft: false

summary: "A .NET web application for managing retained firefighting crew availability."
image: ""
carousel_images: ["ra_logo.png", "retained_kiosk.png"]
class: "project-list__retained-availability"
year: 2015
project_type: "Commercial"
technology: ".NET"
repo: ""

# Images

---

Developed during my internship at [Seed Software](http://www.seedsoftware.co.uk/), Retained Availability is an MVC web application for timetabling crew availability.

I managed the second phase implementation of this project which included gathering requirements, hosting fortnightly teleconferences and sprint planning.

Visual Studio analytical tools were utilised to significantly improve performance and emphasis was placed on writing clean and structured code.