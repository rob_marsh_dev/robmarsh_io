---
title: "Technical Fire Safety"
date: 2018-01-27T09:42:26Z
draft: false

summary: "A .NET web application for managing premise history and performing inspections to ensure fire safety legislations are being met."
image: ""
carousel_images: ["tfs_logo.png", "edit_letter.png", "home_page.png"]
class: "project-list__technical-fire-safety"
year: 2015
project_type: "Commercial"
technology: ".NET"
repo: ""

# Images

---

Developed during my internship at Seed Software, Technical Fire Safety is an MVC web application for managing premise history and performing inspections to ensure fire safety legislations are being met.

Automated unit tests were written alongside the application to ensure structured, robust and modularised code.