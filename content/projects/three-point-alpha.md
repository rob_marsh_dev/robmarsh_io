---
title: "Three Point Alpha"
date: 2018-01-27T09:35:21Z
draft: false

summary: "A WordPress website for a Software Development company."
image: "services.png"
carousel_images: ["services.png", "contact.png", "home.png", "blogs.png"]
colour: "#a1bf35"
year: 2017
project_type: "Commercial"
technology: "WordPress"
repo: "https://bitbucket.org/rob_marsh_dev/three-point-alpha-website"

# Images

---

Three Point Alpha was a bespoke software development company that I formed with two fellow graduates upon finishing university. We needed a website that was going to attract potential customers by highlight our skills and services. This required me to create mockups, develop a company brand and build the website using WordPress.