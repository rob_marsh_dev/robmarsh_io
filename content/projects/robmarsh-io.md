---
title: "robmarsh.io"
date: 2018-01-27T09:41:02Z
draft: false

summary: "I built this website myself using Hugo, a static site generator."
image: "homepage.png"
colour: ""
year: 2018
project_type: "Personal"
technology: "Hugo"
repo: "https://bitbucket.org/rob_marsh_dev/robmarsh_io"

# Images

---

This website is also an example of work I have developed independently. The brief was to create a minimalist site that was fast, easy to update and didn’t require a database. [Hugo](https://gohugo.io/about/what-is-hugo/) proved the perfect tool for generating a speedy static site whilst also offering a number of minimalist themes to which I could incorporate my own personal brand. Layouts make use of HTML and Hugo’s own templating language in order to render content from markdown files. This was my first experience using a CMS that didn’t require a database for storing content and I would highly recommend it for anyone looking to develop a basic site.

The source code for the project can be found below.