---
title: "Json Api"
date: 2018-01-27T09:36:22Z
draft: false

summary: "Support & development on a .NET JSON API."
image: ""
class: "project-list__json-api"
year: 2018
project_type: "Commercial"
technology: ".NET"
repo: ""

# Images

---

As part of ongoing freelance work I perform maintenance and enhancements to a RESTful API used to share core data between several applications in an organisation. The application is written in C# and uses the ASP.NET Web API framework to utilise middleware for parsing of JSON, routing and authentication. The OAuth 2.0 protocol is used for authorisation, whilst the JSON HAL format is used to encompass hyperlinks between resources.