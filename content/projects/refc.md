---
title: "REFC"
date: 2018-01-27T09:37:08Z
draft: false

summary: "A web platform for predicting Premier League scores written in PHP."
image: "predictions.png"
carousel_images: ["home.png", "predictions.png", "chart.png", "fixtures.png"]
colour: "#ff9933"
year: 2016
project_type: "Personal"
technology: "PHP"
repo: "https://bitbucket.org/rob_marsh_dev/refc"

# Images

---

REFC (or the Rob & Emelie Football Competition) is a personal project that myself and a friend developed to replace a paper-based chart we used to predict the Premier League scores. Each week we would write out what we thought the result of each fixture would be and would get points based on whether the prediction had the correct outcome or score. This eventually became a pain to manage and so we decided to develop a web platform where we could make our predictions, input the results and see who was winning at a glance. 

She was preparing to start work at a company who mainly wrote applications using PHP and I was looking to branch away from .NET and so we decided to use the [Laravel](https://laravel.com/) framework to develop the platform. 

Fixtures were imported using [football-data.org](https://www.football-data.org/) and were stored in a SQLite database. The site was hosted on AWS. No winner was ever determined.